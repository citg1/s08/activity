mysql -u root

use music_store;

SELECT * FROM artists;

SELECT * FROM songs;

SELECT * FROM albums;

-- a. Find all the artist that has letter D in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- b. Find all songs that has length of less than 230.
SELECT * FROM songs WHERE length < 230;

-- c.Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT albums.album_title, songs.song_name, songs.length FROM albums
JOIN songs ON albums.id = songs.id;

-- d. Join the artists and albums tables.(Find all albums that has letter A in its name.)
SELECT albums.album_title FROM albums
JOIN artists ON albums.id = artists.id
WHERE albums.album_title LIKE '%A%';

-- e. Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- f. Join the albums and songs tables. (Sort albums from Z-A and sort songs from A-Z.)
SELECT albums.album_title, songs.song_name FROM albums
JOIN songs ON albums.id = songs.id
ORDER BY albums.album_title DESC, songs.song_name ASC;

